import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {
    public static void main(String[] args) {
        checkPassword("passord");
        checkPassword("Passord");
        checkPassword("Passordet");
        checkPassword("Pa55ordet");
        checkPassword("Pa55ordet!");
        checkPassword("12&%2Fs35#¤");
        checkPassword("short");
        checkPassword("Long and invalid");
    }

    static void checkPassword(String password)
    {
        int points = 0; // 0-5 points, show how many of the criteria that is satisfied.

        if (containUppercase(password))
        {
            points ++;
        }

        if (containLowercase(password))
        {
            points ++;
        }

        if (hasEight(password))
        {
            points ++;
        }

        if (containSpecial(password))
        {
            points ++;
        }

        if (containsDigit(password))
        {
            points ++;
        }

        if (invalidPassword(password))
        {
            System.out.println(password + " : Password is invalid! - Password too short or contain space");
        }
        else if (points == 5)
        {
            System.out.println(password + " : Password is strong!");
        }
        else if (points == 3 || points == 4)
        {
            System.out.println(password + " : Password is moderate!");
        }
        else if (points == 1 || points == 2)
        {
            System.out.println(password + " : Password is weak!");
        }
    }


    static boolean containUppercase(String str)
    {
        boolean upperFlag = false;
        char ch;
        for (int i = 0; i < str.length(); i++)
        {
            ch = str.charAt(i);
            if (Character.isUpperCase(ch))
            {
                upperFlag = true;
            }
        }
        return upperFlag;
    }

    static boolean containLowercase(String str)
    {
        boolean lowerFlag = false;
        char ch;
        for (int i = 0; i < str.length(); i++)
        {
            ch = str.charAt(i);
            if (Character.isLowerCase(ch))
            {
                lowerFlag = true;
            }
        }
        return lowerFlag;
    }

    static boolean hasEight(String str)
    {
        boolean hasEight = false;
        if (str.length() > 7)
        {
            hasEight = true;
        }
        return hasEight;
    }

    static boolean containsDigit(String str)
    {
        boolean digitFlag = false;
        char ch;
        for (int i = 0; i < str.length(); i++)
        {
            ch = str.charAt(i);
            if (Character.isDigit(ch))
            {
                digitFlag = true;
            }
        }
        return digitFlag;
    }


    static boolean containSpecial(String str)
    {
        boolean hasSpecialBool = false;

        Pattern special = Pattern.compile("[^a-z0-9 ]", Pattern.CASE_INSENSITIVE);
        Matcher matcher = special.matcher(str);

        boolean containsSymbols = matcher.find();

        if (containsSymbols)
        {
            hasSpecialBool = true;
        }
        return hasSpecialBool;
    }

    static boolean invalidPassword(String str)
    {
        boolean invalidPassword = false;
        if (str.contains(" ") || str.length() < 6)
        {
            invalidPassword = true;
        }
        return invalidPassword;
    }
}
